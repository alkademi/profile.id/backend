﻿using AutoMapper;
using COMPP_API.Controllers;
using COMPP_API.Data;
using COMPP_API.DTO;
using COMPP_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace COMPP_API_Tests
{
    public class AuthControllerTests
    {
        private readonly Mock<COMPPContext> contextStub = new();
        private readonly Mock<IMapper> mapperStub = new();
        private readonly Mock<IConfiguration> configStub = new();
        private DbContextOptions<COMPPContext> _contextOptions;

        private Account dummyAccount = new Account()
        {
            AccountID = 1,
            Email = "testblmkan@gmail.com",
            Name = "nama orang palsu",
            Password = "password123"
        };

        public AuthControllerTests()
        {
            _contextOptions = new DbContextOptionsBuilder<COMPPContext>()
        .UseInMemoryDatabase("AuthControllerTest")
        .ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning))
        .Options;

            using var context = new COMPPContext(_contextOptions);

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }

        [Fact]
        public async void Register_InputValid()
        {
            var dummyNewAccount = new AccountForRegistrationDTO()
            {
                Email = "testblmkan@gmail.com",
                Name = "nama orang palsu",
                Password = "password123"
            };

            var context = CreateContext();
            var controller = new AuthController(mapperStub.Object, context, configStub.Object);

            mapperStub.Setup(repo => repo.Map<Account>(dummyNewAccount)).Returns(dummyAccount);

            var result = await controller.Register(dummyNewAccount);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void Register_EmailInputInvalid()
        {
            var dummyNewAccount = new AccountForRegistrationDTO()
            {
                Email = "",
                Name = "gabisa ya",
                Password = "password123"
            };

            using var context = CreateContext();
            var controller = new AuthController(mapperStub.Object, context, configStub.Object);

            var result = await controller.Register(dummyNewAccount);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void Register_NameInputInvalid()
        {
            var dummyNewAccount = new AccountForRegistrationDTO()
            {
                Email = "test@gmail.com",
                Name = "",
                Password = "password123"
            };

            using var context = CreateContext();
            var controller = new AuthController(mapperStub.Object, context, configStub.Object);

            var result = await controller.Register(dummyNewAccount);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void Login_EmailInputInvalid()
        {
            var context = CreateContext();
            var controller = new AuthController(mapperStub.Object, context, configStub.Object);

            var dummyAccount = new LoginRequestDTO()
            {
                Email = "gaada@gmail.com",
                Password = "password123"
            };

            var result = await controller.Login(dummyAccount);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void Login_PasswordInputInvalid()
        {
            var context = CreateContext();
            var controller = new AuthController(mapperStub.Object, context, configStub.Object);

            var dummyAccount = new LoginRequestDTO()
            {
                Email = "test@gmail.com",
                Password = "bukaninipasswordnya"
            };

            var result = await controller.Login(dummyAccount);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        COMPPContext CreateContext() => new COMPPContext(_contextOptions, (context, modelBuilder) =>
        {
            #region ToInMemoryQuery
            modelBuilder.Entity<Account>()
                .ToInMemoryQuery(() => context.Accounts.Select(b => new Account { Name = b.Name }));
            #endregion
        });
    }
}
