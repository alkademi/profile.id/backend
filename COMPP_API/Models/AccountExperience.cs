﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COMPP_API.Models
{
    public class AccountExperience
    {
        [Key]
        public int ExperienceID { get; set; }
        public int AccountID { get; set; }
        public string ExperienceName { get; set; }
        [Column(TypeName = "Date")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime EndDate { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
    }
}
