﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace COMPP_API.Models
{
    [Index(nameof(Email), IsUnique = true)]
    public class Account
    {
        [Key]
        public int AccountID { get; set; }  
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [JsonIgnore]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        public int Gender { get; set; }
        public string? City { get; set; }
        public string? Picture { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }
        public string? Description { get; set; }

        public ICollection<AccountExperience> AccountExperiences { get; set; }
        public ICollection<AccountEducation> AccountEducations { get; set; }
        //[ForeignKey("FromAccountID")]
        //public virtual ICollection<Connection> Connections { get; set; }
        public virtual ICollection<Connection> FromConnections { get; set; }
        public virtual ICollection<Connection> ToConnections { get; set; }

    }
}
