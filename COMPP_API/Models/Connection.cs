﻿using System.ComponentModel.DataAnnotations;

namespace COMPP_API.Models
{
    public class Connection
    {
        [Key]
        public int ConnectionID { get; set; }
        public int FromAccountID { get; set; }
        public int ToAccountID { get; set; }

        public virtual Account FromAccount { get; set; }
        public virtual Account ToAccount { get; set; }
    }
}
