﻿using AutoMapper;
using COMPP_API.DTO;
using COMPP_API.Models;

namespace COMPP_API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Account, AccountDTO>();
            CreateMap<AccountForRegistrationDTO, Account>();
            CreateMap<AccountForUpdateDTO, Account>();
            CreateMap<Connection, ConnectionDTO>();
            CreateMap<ConnectionForCreationDTO, Connection>();
            CreateMap<AccountEducationDTO, AccountEducation>();
            CreateMap<AccountEducationForUpdateDTO, AccountEducation>();
            CreateMap<AccountExperienceDTO, AccountExperience>();
            CreateMap<AccountExperienceForUpdateDTO, AccountExperience>();
            CreateMap<ProfileInfoDTO, Account>();
            CreateMap<AddProfileDTO, Account>();
            CreateMap<AccountEducation, AccountEducationDTO>();
            CreateMap<AccountExperience, AccountExperienceDTO>();
        }
    }
}
