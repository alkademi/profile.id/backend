﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using COMPP_API.Data;
using COMPP_API.Models;
using COMPP_API.DTO;
using System.Collections.ObjectModel;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace COMPP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly COMPPContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public AuthController(IMapper mapper, COMPPContext context, IConfiguration configuration)
        {
            _mapper = mapper;
            _context = context;
            _configuration = configuration;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(AccountForRegistrationDTO newAccount)
        {
            if (string.IsNullOrWhiteSpace(newAccount.Email) || string.IsNullOrWhiteSpace(newAccount.Name) || string.IsNullOrWhiteSpace(newAccount.Password))
            {
                return BadRequest("One of the required fields is empty");
            }

            if (_context.Accounts.Any(e => e.Email == newAccount.Email))
            {
                return BadRequest("Email already exists");
            }

            var account = _mapper.Map<Account>(newAccount);
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();

            return Ok(account);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequestDTO user)
        {
            var account = _context.Accounts
                                    .Where(a => a.Email == user.Email)
                                    .FirstOrDefault();

            // Account doesn't exist or password is wrong
            if (account == null || account.Password != user.Password)
            {
                return BadRequest("Username and password don't match");
            }

            // Login successful, send token to client
            string token = createToken(account.AccountID.ToString());
            LoginResponseDTO loginResponse = new()
            {
                Token = token,
                ID = account.AccountID,
                Email = account.Email,
                Name = account.Name,
                Gender = account.Gender,
                City = account.City
            };

            return Ok(loginResponse);
        }

        [HttpGet("test-auth"), Authorize]
        public string TestAuth()
        {
            var mess = "Authentication works";
            return mess;
        }

        private string createToken(string username)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, username)
            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
    
            return jwt;
        }
    }
}