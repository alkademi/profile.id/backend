#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using COMPP_API.Data;
using COMPP_API.Models;
using COMPP_API.DTO;
using System.Collections.ObjectModel;
using AutoMapper;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;

namespace COMPP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly COMPPContext _context;
        private readonly IMapper _mapper;

        public AccountsController(IMapper mapper, COMPPContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Accounts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccountDTO>>> GetAccounts(string name, string job, string company)
        {
            Debug.WriteLine(name);
            IEnumerable<Account> accounts = await _context.Accounts.ToListAsync();

            // Filter name
            if (name != null)
            {
                accounts = accounts.Where(a => a.Name.ToLower().Contains(name.ToLower()));
            }

            // Filter job
            if (job != null)
            {
                IEnumerable<int> jobAccountIDs = _context.AccountExperiences
                    .Where(exp => exp.Title.ToLower().Contains(job.ToLower()))
                    .Select(exp => exp.AccountID);

                accounts = accounts.Where(a => jobAccountIDs.Any(id => id == a.AccountID));
            }

            // Filter company
            if (company != null)
            {
                IEnumerable<int> companyAccountIDs = _context.AccountExperiences
                    .Where(exp => exp.ExperienceName.ToLower().Contains(company.ToLower()))
                    .Select(exp => exp.AccountID);

                accounts = accounts.Where(a => companyAccountIDs.Any(id => id == a.AccountID));
            }

            var mappedAccounts = _mapper.Map<IEnumerable<AccountDTO>>(accounts).ToList();

            return mappedAccounts;
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AccountDTO>> GetAccount(int id)
        {
            var account = await _context.Accounts.FindAsync(id);

            if (account == null)
            {
                return NotFound();
            }

            var mappedAccount = _mapper.Map<AccountDTO>(account);

            return mappedAccount;
        }

        private ProfileInfoDTO accountToProfile(Account account)
        {
            // Get all educations for the given id
            var educations = _context.AccountEducations
                                                .Where(a => a.AccountID == account.AccountID)
                                                .ToList();

            var mappedEducations = _mapper.Map<IEnumerable<AccountEducationDTO>>(educations).ToList();

            // Get all experiences for the given id
            var experiences = _context.AccountExperiences
                                                .Where(a => a.AccountID == account.AccountID)
                                                .ToList();

            var mappedExperiences = _mapper.Map<IEnumerable<AccountExperienceDTO>>(experiences).ToList();

            ProfileInfoDTO profileInfo = new ProfileInfoDTO
            {
                Id = account.AccountID,
                Name = account.Name,
                Gender = account.Gender,
                City = account.City,
                Picture = account.Picture,
                DateOfBirth = account.DateOfBirth,
                Description = account.Description,
                Educations = mappedEducations.ToArray(),
                Experiences = mappedExperiences.ToArray()
            };

            return profileInfo;
        }

        private ConnectionProfileDTO connectionToProfile (Connection connection)
        {
            Account toAccount = _context.Accounts.Find(connection.ToAccountID);

            return new ConnectionProfileDTO
            {
                ConnectionID = connection.ConnectionID,
                ProfileInfo = accountToProfile(toAccount)
            };
        }

        // GET: api/Accounts/5/connections
        [HttpGet("{id}/connections")]
        public ActionResult<IEnumerable<ConnectionProfileDTO>> GetAccountConnections(int id)
        {
            var accountConnections = _context.Connections.Where(c => c.FromAccountID == id).ToList();

            IEnumerable<ConnectionProfileDTO> connectionProfile = accountConnections.Select(c => connectionToProfile(c));

            return Ok(connectionProfile);
        }

        // PUT: api/Accounts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}"), Authorize]
        public async Task<IActionResult> PutAccount(int id, AccountForUpdateDTO account)
        {
            var accountToUpdate = await _context.Accounts.FindAsync(id);

            if (accountToUpdate == null)
            {
                return BadRequest();
            }

            var mappedAccount = _mapper.Map(account, accountToUpdate);

            _context.Entry(accountToUpdate).State = EntityState.Modified;

            try
            {
                _context.Update(mappedAccount);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/Accounts/5/addprofile
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}/addprofile")]
        public async Task<ActionResult<ProfileInfoDTO>> AddProfile(int id, AddProfileDTO profileInfo)
        {
            var accountToUpdate = await _context.Accounts.FindAsync(id);

            if (accountToUpdate == null)
            {
                return BadRequest();
            }

            var educations = _mapper.Map<AccountEducationDTO[], AccountEducation[]>(profileInfo.Educations);
            var expereiences = _mapper.Map<AccountExperienceDTO[], AccountExperience[]>(profileInfo.Experiences);

            _context.AccountEducations.AddRange(educations);
            _context.AccountExperiences.AddRange(expereiences);

            var mappedAccount = _mapper.Map(profileInfo, accountToUpdate);

            _context.Entry(accountToUpdate).State = EntityState.Modified;

            try
            {
                _context.Update(mappedAccount);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Accounts/5
        [HttpDelete("{id}"), Authorize]
        public async Task<IActionResult> DeleteAccount(int id)
        {
            var account = await _context.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AccountExists(int id)
        {
            return _context.Accounts.Any(e => e.AccountID == id);
        }
    }
}
