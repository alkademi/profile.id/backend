#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using COMPP_API.Data;
using COMPP_API.Models;
using AutoMapper;
using COMPP_API.DTO;

namespace COMPP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountEducationsController : ControllerBase
    {
        private readonly COMPPContext _context;
        private readonly IMapper _mapper;

        public AccountEducationsController(IMapper mapper, COMPPContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/AccountEducations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccountEducation>>> GetAccountEducations()
        {
            return await _context.AccountEducations.ToListAsync();
        }

        // GET: api/AccountEducations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AccountEducation>> GetAccountEducation(int id)
        {
            var accountEducation = await _context.AccountEducations.FindAsync(id);

            if (accountEducation == null)
            {
                return NotFound();
            }

            return accountEducation;
        }

        // PUT: api/AccountEducations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccountEducation(int id, AccountEducationForUpdateDTO accountEducation)
        {
            var accountEducationToUpdate = await _context.AccountEducations.FindAsync(id);

            if (accountEducationToUpdate == null)
            {
                return NotFound();
            }

            var mappedAccountEducation = _mapper.Map(accountEducation, accountEducationToUpdate);

            _context.Entry(mappedAccountEducation).State = EntityState.Modified;

            try
            {
                _context.Update(mappedAccountEducation);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountEducationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AccountEducations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<AccountEducation>> PostAccountEducation(AccountEducationDTO accountEducation)
        {
            var accountEducationToCreate = _mapper.Map<AccountEducation>(accountEducation);
            _context.AccountEducations.Add(accountEducationToCreate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccountEducation", new { id = accountEducationToCreate.EducationID }, accountEducation);
        }

        // DELETE: api/AccountEducations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccountEducation(int id)
        {
            var accountEducation = await _context.AccountEducations.FindAsync(id);
            if (accountEducation == null)
            {
                return NotFound();
            }

            _context.AccountEducations.Remove(accountEducation);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AccountEducationExists(int id)
        {
            return _context.AccountEducations.Any(e => e.EducationID == id);
        }
    }
}
