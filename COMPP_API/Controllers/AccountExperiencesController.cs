#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using COMPP_API.Data;
using COMPP_API.Models;
using AutoMapper;
using COMPP_API.DTO;

namespace COMPP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountExperiencesController : ControllerBase
    {
        private readonly COMPPContext _context;
        private readonly IMapper _mapper;

        public AccountExperiencesController(IMapper mapper, COMPPContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/AccountExperiences
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccountExperience>>> GetAccountExperiences()
        {
            return await _context.AccountExperiences.ToListAsync();
        }

        // GET: api/AccountExperiences/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AccountExperience>> GetAccountExperience(int id)
        {
            var accountExperience = await _context.AccountExperiences.FindAsync(id);

            if (accountExperience == null)
            {
                return NotFound();
            }

            return accountExperience;
        }

        // PUT: api/AccountExperiences/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccountExperience(int id, AccountExperienceForUpdateDTO accountExperience)
        {
            var accountExperienceToUpdate = await _context.AccountExperiences.FindAsync(id);

            if (accountExperienceToUpdate == null)
            {
                return NotFound();
            }

            var mappedAccountExperience = _mapper.Map(accountExperience, accountExperienceToUpdate);

            _context.Entry(mappedAccountExperience).State = EntityState.Modified;

            try
            {
                _context.Update(mappedAccountExperience);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExperienceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AccountExperiences
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<AccountExperienceDTO>> PostAccountExperience(AccountExperienceDTO accountExperience)
        {
            var accountExperienceToCreate = _mapper.Map<AccountExperience>(accountExperience);
            _context.AccountExperiences.Add(accountExperienceToCreate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccountExperience", new { id = accountExperienceToCreate.ExperienceID }, accountExperience);
        }

        // DELETE: api/AccountExperiences/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccountExperience(int id)
        {
            var accountExperience = await _context.AccountExperiences.FindAsync(id);
            if (accountExperience == null)
            {
                return NotFound();
            }

            _context.AccountExperiences.Remove(accountExperience);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AccountExperienceExists(int id)
        {
            return _context.AccountExperiences.Any(e => e.ExperienceID == id);
        }
    }
}
