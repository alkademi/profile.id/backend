using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using COMPP_API.Data;
using COMPP_API.Models;
using COMPP_API.DTO;
using System.Collections.ObjectModel;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace COMPP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class ProfileController : ControllerBase
    {
        private readonly COMPPContext _context;
        private readonly IMapper _mapper;

        public ProfileController(IMapper mapper, COMPPContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Profile/5
        [HttpGet("{id}"), Authorize]
        public async Task<ActionResult<ProfileInfoDTO>> GetProfile(int id)
        {
            // Get profile information
            var account = await _context.Accounts.FindAsync(id);

            if (account != null)
            {
                // Get all educations for the given id
                var educations = await _context.AccountEducations
                                                    .Where(a => a.AccountID == id)
                                                    .ToListAsync();

                var mappedEducations = _mapper.Map<IEnumerable<AccountEducationDTO>>(educations).ToList();

                // Get all experiences for the given id
                var experiences = await _context.AccountExperiences
                                                    .Where(a => a.AccountID == id)
                                                    .ToListAsync();

                var mappedExperiences = _mapper.Map<IEnumerable<AccountExperienceDTO>>(experiences).ToList();

                ProfileInfoDTO profileInfo = new ProfileInfoDTO
                {
                    Id = account.AccountID,
                    Email = account.Email,
                    Name = account.Name,
                    Gender = account.Gender,
                    City = account.City,
                    Picture = account.Picture,
                    DateOfBirth = account.DateOfBirth,
                    Description = account.Description,
                    Educations = mappedEducations.ToArray(),
                    Experiences = mappedExperiences.ToArray()
                };

                return Ok(profileInfo);
            }

            return BadRequest("Account does not exist");
        }

    }
}