#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using COMPP_API.Data;
using COMPP_API.Models;
using COMPP_API.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace COMPP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConnectionsController : ControllerBase
    {
        private readonly COMPPContext _context;
        private readonly IMapper _mapper;

        public ConnectionsController(IMapper mapper, COMPPContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Connections
        [HttpGet, Authorize]
        public async Task<ActionResult<IEnumerable<ConnectionDTO>>> GetConnections()
        {
            var connections = await _context.Connections.ToListAsync();
            var mappedConnections = _mapper.Map<IEnumerable<ConnectionDTO>>(connections).ToList();
            return mappedConnections;
        }

        // GET: api/Connections/5
        [HttpGet("{id}"), Authorize]
        public async Task<ActionResult<ConnectionDTO>> GetConnection(int id)
        {
            var connection = await _context.Connections.FindAsync(id);

            if (connection == null)
            {
                return NotFound();
            }

            return _mapper.Map<ConnectionDTO>(connection);
        }

        // GET: api/Connections/check/5/6
        [HttpGet("check/{from}/{to}"), Authorize]
        public ActionResult<ConnectionDTO> CheckConnection(int from, int to)
        {
            var connections = _context.Connections.Where(c => c.FromAccountID == from && c.ToAccountID == to).ToList();

            Connection connection = null;
            if (connections.Count() > 0)
            {
                connection = connections.First();
            }

            if (connection == null)
            {
                return NotFound(new {message = "Connection does not exist"});
            }

            return Ok(_mapper.Map<ConnectionDTO>(connection));
        }

        // POST: api/Connections
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost, Authorize]
        public async Task<ActionResult<ConnectionDTO>> PostConnection(ConnectionForCreationDTO connection)
        {
            var fromAccount = await _context.Accounts.FindAsync(connection.FromAccountID);
            var toAccount = await _context.Accounts.FindAsync(connection.ToAccountID);

            if (connection.FromAccountID == connection.ToAccountID || fromAccount == null || toAccount == null)
            {
                return BadRequest();
            }

            if (_context.Connections.Any(c => c.FromAccountID == connection.FromAccountID && c.ToAccountID == connection.ToAccountID))
            {
                return Conflict();
            }

            var connectionToCreate = _mapper.Map<Connection>(connection);

            _context.Connections.Add(connectionToCreate);
            await _context.SaveChangesAsync();
            var connectionToReturn = _mapper.Map<ConnectionDTO>(connectionToCreate);

            return CreatedAtAction("GetConnection", new { id = connectionToCreate.ConnectionID }, connectionToReturn);
        }

        // DELETE: api/Connections/5
        [HttpDelete("{id}"), Authorize]
        public async Task<IActionResult> DeleteConnection(int id)
        {
            var connection = await _context.Connections.FindAsync(id);
            if (connection == null)
            {
                return NotFound();
            }

            _context.Connections.Remove(connection);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
