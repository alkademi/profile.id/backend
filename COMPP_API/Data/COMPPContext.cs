﻿using COMPP_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace COMPP_API.Data
{
    public class COMPPContext : DbContext
    {
        private readonly Action<COMPPContext, ModelBuilder> _modelCustomizer;

        public COMPPContext(DbContextOptions<COMPPContext> options)
            : base(options)
        {
        }

        public COMPPContext(DbContextOptions<COMPPContext> options, Action<COMPPContext, ModelBuilder> modelCustomizer = null)
       : base(options)
        {
            _modelCustomizer = modelCustomizer;
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountExperience> AccountExperiences { get; set; }
        public DbSet<AccountEducation> AccountEducations { get; set; }
        public DbSet<Connection> Connections { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(
                    @"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Connection>()
                 .HasOne(m => m.FromAccount)
                 .WithMany(m => m.FromConnections)
                 .HasForeignKey(m => m.FromAccountID)
                 .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Connection>()
                 .HasOne(m => m.ToAccount)
                 .WithMany(m => m.ToConnections)
                 .HasForeignKey(m => m.ToAccountID)
                 .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
