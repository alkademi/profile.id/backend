﻿using COMPP_API.Data;
using Microsoft.EntityFrameworkCore;

namespace COMPP_API
{
    public static class Extensions
    {
        public static IHost MigrateDatabase(this IHost webHost)
        {
            // Manually run any pending migrations if configured to do so.

            var serviceScopeFactory = (IServiceScopeFactory)webHost.Services.GetService(typeof(IServiceScopeFactory));

            using (var scope = serviceScopeFactory.CreateScope())
            {
                var services = scope.ServiceProvider;
                var dbContext = services.GetRequiredService<COMPPContext>();

                dbContext.Database.Migrate();
            }

            return webHost;
        }
    }
}
