﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace COMPP_API.Migrations
{
    public partial class AddGenderColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "Accounts",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Accounts");
        }
    }
}
