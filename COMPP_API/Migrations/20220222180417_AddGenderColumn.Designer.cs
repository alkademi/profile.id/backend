﻿// <auto-generated />
using System;
using COMPP_API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace COMPP_API.Migrations
{
    [DbContext(typeof(COMPPContext))]
    [Migration("20220222180417_AddGenderColumn")]
    partial class AddGenderColumn
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("COMPP_API.Models.Account", b =>
                {
                    b.Property<int>("AccountID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("AccountID"));

                    b.Property<DateTime>("DateOfBirth")
                        .HasColumnType("Date");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("Gender")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Picture")
                        .HasColumnType("text");

                    b.HasKey("AccountID");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("COMPP_API.Models.AccountEducation", b =>
                {
                    b.Property<int>("EducationID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("EducationID"));

                    b.Property<int>("AccountID")
                        .HasColumnType("integer");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("Date");

                    b.Property<string>("SchoolName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("Date");

                    b.HasKey("EducationID");

                    b.HasIndex("AccountID");

                    b.ToTable("AccountEducations");
                });

            modelBuilder.Entity("COMPP_API.Models.AccountExperience", b =>
                {
                    b.Property<int>("ExperienceID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("ExperienceID"));

                    b.Property<int>("AccountID")
                        .HasColumnType("integer");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("Date");

                    b.Property<string>("ExperienceName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("Date");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("ExperienceID");

                    b.HasIndex("AccountID");

                    b.ToTable("AccountExperiences");
                });

            modelBuilder.Entity("COMPP_API.Models.Connection", b =>
                {
                    b.Property<int>("ConnectionID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("ConnectionID"));

                    b.Property<int>("FromAccountID")
                        .HasColumnType("integer");

                    b.Property<int>("ToAccountID")
                        .HasColumnType("integer");

                    b.HasKey("ConnectionID");

                    b.HasIndex("FromAccountID");

                    b.HasIndex("ToAccountID");

                    b.ToTable("Connections");
                });

            modelBuilder.Entity("COMPP_API.Models.AccountEducation", b =>
                {
                    b.HasOne("COMPP_API.Models.Account", null)
                        .WithMany("AccountEducations")
                        .HasForeignKey("AccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("COMPP_API.Models.AccountExperience", b =>
                {
                    b.HasOne("COMPP_API.Models.Account", null)
                        .WithMany("AccountExperiences")
                        .HasForeignKey("AccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("COMPP_API.Models.Connection", b =>
                {
                    b.HasOne("COMPP_API.Models.Account", "FromAccount")
                        .WithMany("FromConnections")
                        .HasForeignKey("FromAccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("COMPP_API.Models.Account", "ToAccount")
                        .WithMany("ToConnections")
                        .HasForeignKey("ToAccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("FromAccount");

                    b.Navigation("ToAccount");
                });

            modelBuilder.Entity("COMPP_API.Models.Account", b =>
                {
                    b.Navigation("AccountEducations");

                    b.Navigation("AccountExperiences");

                    b.Navigation("FromConnections");

                    b.Navigation("ToConnections");
                });
#pragma warning restore 612, 618
        }
    }
}
