﻿namespace COMPP_API.DTO
{
    public class ConnectionForCreationDTO
    {
        public int FromAccountID { get; set; }
        public int ToAccountID { get; set; }
    }
}
