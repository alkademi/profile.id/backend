﻿namespace COMPP_API.DTO
{
    public class ConnectionProfileDTO
    {
        public int ConnectionID { get; set; }
        public ProfileInfoDTO ProfileInfo { get; set; }
    }
}
