﻿using System.ComponentModel.DataAnnotations.Schema;

namespace COMPP_API.DTO
{
    public class AddProfileDTO
    {
        public string Name { get; set; }
        public int Gender { get; set; }
        public string? City { get; set; }
        public string? Picture { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }
        public string? Description { get; set; }
        public AccountEducationDTO[] Educations { get; set; }
        public AccountExperienceDTO[] Experiences { get; set; }
    }
}
