﻿namespace COMPP_API.DTO
{
    public class ConnectionDTO
    {
        public int ConnectionID { get; set; }
        public int FromAccountID { get; set; }
        public int ToAccountID { get; set; }
    }
}
