namespace COMPP_API.DTO
{
    public class LoginResponseDTO
    {
        public string Token { get; set; }
        public int ID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
    }
}
