﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COMPP_API.DTO
{
    public class AccountEducationDTO
    {
        public int EducationID { get; set; }
        public int AccountID { get; set; }
        public string SchoolName { get; set; }
        [Column(TypeName = "Date")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime EndDate { get; set; }
        public string? Description { get; set; }
    }
}
