﻿using System.ComponentModel.DataAnnotations.Schema;

namespace COMPP_API.DTO
{
    public class AccountDTO
    {
        public int AccountID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string? City { get; set; }
        public string? Picture { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }
        public string? Description { get; set; }
    }
}
